
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';

var username = "ois.seminar";
var password = "ois4fri";


/**
 * Generiranje niza za avtorizacijo na podlagi uporabniškega imena in gesla,
 * ki je šifriran v niz oblike Base64
 *
 * @return avtorizacijski niz za dostop do funkcionalnost
 */
function getAuthorization() {
  return "Basic " + btoa(username + ":" + password);
}
function dodajMeritveVitalnihZnakov(ehrId, datumUra, telesnaVisina, telesnaTeza, telesnaTemperatura,sistolicniKrviTlak, diastolicniKrvniTlak){
  var authorization = getAuthorization();
  if(!ehrId || ehrId.trim().length==0){
    $("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
  }
  else{
    $.ajaxSetup({
    headers: {
        "Authorization": authorization
    }
});
  var podatki = {
    // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrviTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,

  };
  
  var parametri= {
    "ehrId" : ehrId,
    templateId: 'Vital Signs',
    format: 'FLAT',
  };
  
  $.ajax({
    url: baseUrl + "/composition?" + $.param(parametri),
    type: 'POST',
    contentType: 'application/json',
    data: JSON.stringify(podatki),
    success: function (res) {
       $("#dodajMeritveVitalnihZnakovSporocilo").html(
              "<span class='obvestilo label label-success fade-in'>" +
              res.meta.href + ".</span>");
    }
});

    
  }
  
  
}


/**
 * Generator podatkov za novega pacienta, ki bo uporabljal aplikacijo. Pri
 * generiranju podatkov je potrebno najprej kreirati novega pacienta z
 * določenimi osebnimi podatki (ime, priimek in datum rojstva) ter za njega
 * shraniti nekaj podatkov o vitalnih znakih.
 * @param stPacienta zaporedna številka pacienta (1, 2 ali 3)
 * @return ehrId generiranega pacienta
 */

function generirajPodatke(stPacienta) {
  var authorization = getAuthorization();
  ehrId = "";
  var ime="";
  var priimek="";
  var datumRojstva="";
  $("#header").html("");
  
  if(stPacienta == 1){
    ime = "Robert";
    priimek = "Sportnik";
    datumRojstva = "1990-03-09T00:00:00.000Z";
  }
  else if(stPacienta == 2){
    ime= "Borut";
    priimek= "Star-Bolan";
    datumRojstva = "1940-07-30T23:30:45.000Z";
  }
  else if(stPacienta == 3){
    ime = "Martin";
    priimek= "Mlad-Poskodovan";
    datumRojstva = "2007-07-30T23:30:45.000Z";
  }
  $.ajaxSetup({
    headers: {
        "Authorization": authorization
    }
});
$.ajax({
    url: baseUrl + "/ehr",
    type: 'POST',
    success: function (data) {
        var ehrId = data.ehrId;
        $("#header").html($("#header").html()+ "<br/>"+ "EHR: " + ehrId);
        // build party data
        var partyData = {
            firstNames: ime,
            lastNames: priimek,
            dateOfBirth: datumRojstva,
            partyAdditionalInfo: [
                {
                    key: "ehrId",
                    value: ehrId
                }
            ]
        };
        $.ajax({
            url: baseUrl + "/demographics/party",
            type: 'POST',
            contentType: 'application/json',
            data: JSON.stringify(partyData),
            success: function (party) {
                if (party.action == 'CREATE') {
                }
            }
        });
        if(stPacienta == 1){
          dodajMeritveVitalnihZnakov(ehrId, "2018-05-20T08:30:00.000Z", "180", "80", "37", "80", "100");
          dodajMeritveVitalnihZnakov(ehrId, "2018-06-20T08:30:00.000Z", "180", "81", "36.9", "75", "103");
          dodajMeritveVitalnihZnakov(ehrId, "2018-07-20T08:38:00.000Z", "180", "79", "37", "80", "100");
          dodajMeritveVitalnihZnakov(ehrId, "2018-08-20T09:30:00.000Z", "180", "82", "37", "83", "98");
          
          
        }
        else if(stPacienta == 2){
          dodajMeritveVitalnihZnakov(ehrId, "2018-05-20T08:30:00.000Z", "168", "67", "37.7", "89", "120");
          dodajMeritveVitalnihZnakov(ehrId, "2018-06-20T08:30:00.000Z", "168", "67", "37.7", "93", "130");
          dodajMeritveVitalnihZnakov(ehrId, "2018-07-20T09:30:00.000Z", "168", "67", "37.7", "97", "140");
          
        }
        
        else if(stPacienta == 3){
          dodajMeritveVitalnihZnakov(ehrId, "2018-01-20T14:30:00.000Z", "150", "43", "36.7", "80", "100");
          dodajMeritveVitalnihZnakov(ehrId, "2018-02-23T14:30:00.000Z", "151", "43", "36.8", "84", "105");
          dodajMeritveVitalnihZnakov(ehrId, "2018-03-23T14:30:00.000Z", "152", "45", "36.9", "86", "106");
          dodajMeritveVitalnihZnakov(ehrId, "2018-04-23T14:30:00.000Z", "152", "49", "36.7", "85", "108");
          
        }
        
    }
});

  return ehrId;
}


// TODO: Tukaj implementirate funkcionalnost, ki jo podpira vaša aplikacija
function generiraj(){
  generirajPodatke(1);
  generirajPodatke(2);
  generirajPodatke(3);
  console.log("dela");
}

function vrniMeritveVitalnihZnakov(stevilo){
  if(stevilo == 1){
  console.log("potegnimikurac");
  var ehrId = document.getElementById("mojselect").value;
  }
  else{
    var ehrId = document.getElementById("rocniEHR").value;
  }
  console.log(ehrId);
  $("#predresult").text('');
  $("#result").text('');
  $("#osebniPodatki").text('');
  $("#predtemperatura").text('');
  $("#temperatura").text('');
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (data) {
        var party = data.party;
        $("#predresult").append("Meritve telesne teže za " + party.firstNames + ' ' +
                                                         party.lastNames);
        $("#osebniPodatki").append("Ime: " + party.firstNames + "<br>" + "Priimek: " + party.lastNames + "<br>" + "Datum rojstva: " + party.dateOfBirth);
        $("#predtemperatura").append("Meritve tempurature za " + party.firstNames + ' ' + party.lastNames);
    }
});
$.ajax({
    url: baseUrl + "/view/" + ehrId + "/weight",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {
        for (var i in res) {
            $("#result").append(res[i].time + ': ' + res[i].weight + res[i].unit + "<br>");
        }
    }
});
  
$.ajax({
    url: baseUrl + "/view/" + ehrId + "/body_temperature",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {
        for (var i in res) {
         
            $("#temperatura").append(res[i].time + ': ' + res[i].temperature + res[i].unit + "<br>");
        }
    }
});
  
  
  
  
  
  
  
  
  
  
}

function narisiGraf() {
    console.log("se klie");
  $('#lineChart').remove(); // this is my <canvas> element
  $('#graph-container').append('<canvas id="lineChart" height="400" length="400"><canvas>');
const CHART = document.getElementById("lineChart");
var ehrId = document.getElementById("mojselect").value;

console.log(ehrId);

if(ehrId == "4e57f1e8-7c89-485b-8342-7891fe57dec0"){
 
  console.log("swag");
let lineChart = new Chart(CHART, {
  type : 'line',
   data: {
     labels:["Maj", "Junij", "Julij", "Avgust"],
     datasets: [
       {
         label: "prikaz sistolicnega krvnega tlaka Roberta Sportnika",
         fill:false,
         lineTension: 0.1,
         backgroundColor: "rgba(75, 192, 192, 0.4)",
         borderColor: "rgba(75, 192, 192, 1)",
         borderCapStyle: 'butt',
         borderDash : [],
         borderDashOffset: 0.0,
         borderJoinStyle: 'mitter',
         pointBorderColor: "rgba(75, 192, 192, 1)",
         pointBackgroundColor: "#fff",
         pointBorderWidth: 1,
         pointHoverRadius: 5,
         pointHoverBackgroundColor: "rgba(75, 192, 192, 1)",
         pointHoverBorderColor: "rgba(220, 220, 220, 1)",
         pointHoverBorderWidth: 2,
         pointRadius: 1,
         pointHitRadius: 10,
         data: [80, 75, 80, 83],
       }
       ]
   }
});

}
if(ehrId== "f0353a09-a097-4487-a49f-8c705f08f7bf"){
  let lineChart = new Chart(CHART, {
  type : 'line',
   data: {
     labels:["Maj", "Junij", "Julij"],
     datasets: [
       {
         label: "prikaz sistolicnega krvnega tlaka Boruta Star-Bolanega",
         fill:false,
         lineTension: 0.1,
         backgroundColor: "rgba(75, 192, 192, 0.4)",
         borderColor: "rgba(75, 192, 192, 1)",
         borderCapStyle: 'butt',
         borderDash : [],
         borderDashOffset: 0.0,
         borderJoinStyle: 'mitter',
         pointBorderColor: "rgba(75, 192, 192, 1)",
         pointBackgroundColor: "#fff",
         pointBorderWidth: 1,
         pointHoverRadius: 5,
         pointHoverBackgroundColor: "rgba(75, 192, 192, 1)",
         pointHoverBorderColor: "rgba(220, 220, 220, 1)",
         pointHoverBorderWidth: 2,
         pointRadius: 1,
         pointHitRadius: 10,
         data: [89,93,97],
       }
       ]
   }
});

}
if(ehrId== "6f682431-621e-446a-925d-36a4cbf0d0d0"){
    var tabelca = ["Januar", "Februar", "Marec", "April"];
    console.log(tabelca);
  let lineChart = new Chart(CHART, {
  type : 'line',
   data: {
     labels: tabelca,
     datasets: [
       {
         label: "prikaz sistolicnega krvnega tlaka Martin Mlad-Poskodovanega",
         fill:false,
         lineTension: 0.1,
         backgroundColor: "rgba(75, 192, 192, 0.4)",
         borderColor: "rgba(75, 192, 192, 1)",
         borderCapStyle: 'butt',
         borderDash : [],
         borderDashOffset: 0.0,
         borderJoinStyle: 'mitter',
         pointBorderColor: "rgba(75, 192, 192, 1)",
         pointBackgroundColor: "#fff",
         pointBorderWidth: 1,
         pointHoverRadius: 5,
         pointHoverBackgroundColor: "rgba(75, 192, 192, 1)",
         pointHoverBorderColor: "rgba(220, 220, 220, 1)",
         pointHoverBorderWidth: 2,
         pointRadius: 1,
         pointHitRadius: 10,
         data: [80,84,86,85],
       }
       ]
   }
});

    
}


}

function narisiRocniGraf(){
   $('#lineChart').remove(); // this is my <canvas> element
  $('#graph-container').append('<canvas id="lineChart" height="400" length="400"><canvas>');
const CHART = document.getElementById("lineChart");
    var ehrId = document.getElementById("rocniEHR").value;
    console.log(ehrId);
    var tabelaDatumov = [];
    var tabelaPritiskov = [];
    var ime;
    var priimek;
    $.ajax({
    url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (data) {
        var party = data.party;
       ime = party.firstNames;
       priimek = party.lastNames;
       console.log(ime);
    }
});   
    $.ajax({
    url: baseUrl + "/view/" + ehrId + "/blood_pressure",
    type: 'GET',
    headers: {
        "Authorization": getAuthorization()
    },
    success: function (res) {
        for (var i in res) {
            var cas = res[i].time;
            
            var tlak = res[i].systolic;
            tabelaDatumov[i] = cas;
            tabelaPritiskov.push(tlak);
            
        }
        let lineChart = new Chart(CHART, {
  type : 'line',
   data: {
     labels: tabelaDatumov,
     datasets: [
       {
         label: "prikaz sistolicnega krvnega tlaka pacienta",
         fill:false,
         lineTension: 0.1,
         backgroundColor: "rgba(75, 192, 192, 0.4)",
         borderColor: "rgba(75, 192, 192, 1)",
         borderCapStyle: 'butt',
         borderDash : [],
         borderDashOffset: 0.0,
         borderJoinStyle: 'mitter',
         pointBorderColor: "rgba(75, 192, 192, 1)",
         pointBackgroundColor: "#fff",
         pointBorderWidth: 1,
         pointHoverRadius: 5,
         pointHoverBackgroundColor: "rgba(75, 192, 192, 1)",
         pointHoverBorderColor: "rgba(220, 220, 220, 1)",
         pointHoverBorderWidth: 2,
         pointRadius: 1,
         pointHitRadius: 10,
         data: tabelaPritiskov,
       }
       ]
   }
});
    }
});
console.log(ime);
console.log(priimek);
  
    

}
//implementacija API klica

/* global $ */

$(document).ready(function() {
  var baseUrl = "https://teaching.lavbic.net/OIS/chat/api";
  var user = {id: 50, name: "Uporabnik"}; // TODO: vnesi svoje podatke
  var nextMessageId = 0;
  var currentRoom = "Skedenj";


  // Naloži seznam sob
  $.ajax({
    url: baseUrl + "/rooms",
    type: "GET",
    success: function (data) {
      for (var i in data) {
        $("#rooms").append(" \
          <li class='media'> \
            <div class='media-body room' style='cursor: pointer;'> \
              <div class='media'> \
                <a class='pull-left' href='#'> \
                  <img class='media-object img-circle' src='knjiznice/img/" + data[i] + ".jpg' /> \
                </a> \
                <div class='media-body'> \
                  <h5>" + data[i] + "</h5> \
                </div> \
              </div> \
            </div> \
          </li>");
      }
      $(".room").click(changeRoom);
    }
  });


  // TODO: Naloga
  // Definicija funkcije za pridobivanje pogovorov, ki se samodejno ponavlja na 5 sekund
var updateChat = function() {
  $.ajax({
    url: baseUrl + "/messages/" + currentRoom + "/" + nextMessageId,
    type: "GET",
    success: function(data) {
      for(var i in data){
        var message=data[i];
        $("#messages").append(" \
         <li class='media'> \
            <div class='media-body'> \
              <div class='pull-left' href='#'> \
                <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + message.user.id + ".jpg'> \
                </a> \
                <div class='media-body'> \
                <small class='text-muted'>" + message.user.name  + " | " + message.time  + "</small> <br> \
                 " + message.text + " \
                 <hr> \
                  </div> \
                </div> \
              </div> \
            </li>");
        
            nextMessageId = message.id + 1;
      }
      setTimeout(function() {updateChat()}, 5000);
    }
    
    
    
  });
  
  
};

  // Klic funkcije za začetek nalaganja pogovorov
  updateChat();

  // TODO: Naloga
  // Definicija funkcije za posodabljanje seznama uporabnikov, ki se samodejno ponavlja na 5 sekund
  var updateUsers = function() {
    $.ajax({
      url: baseUrl + "/users/" + currentRoom,
      type: "GET",
      success: function (data) {
        $("#users").html("");
        for (var i in data) {
          var user = data[i];
          $("#users").append(" \
            <li class='media'> \
             <div class='media-body'> \
                <div class='media'> \
                   <a class='pull-left' href='#'> \
                     <img class='media-object img-circle' src='https://randomuser.me/api/portraits/men/" + user.id + ".jpg' /> \
                       </a> \
                       <div class='media-body' > \
                        <h5>" + user.name + "</h5> \
                        </div> \
                </div> \
              </div> \
            </li>");
        }
         setTimeout(function() {updateUsers()}, 5000);
      }
    });
  };
         
  // Klic funkcije za začetek posodabljanja uporabnikov
  updateUsers();

  // TODO: Naloga
  // Definicija funkcije za pošiljanje sporočila
   var sendMessage = function () {
     $.ajax({
      url: baseUrl + "/messages/" + currentRoom,
      type: "POST",
      contentType: 'application/json',
       data: JSON.stringify({user: user, text: $("#message").val()}),
        success: function (data) {
           $("#message").val("");
        },
      error: function(err) {
        alert("Prišlo je do napake pri pošiljanju sporočila. Prosimo poskusite znova!");
      }
    });
  };
  // On Click handler za pošiljanje sporočila
$("#send").click(sendMessage);

  // TODO: Naloga
  // Definicija funkcije za menjavo sobe (izbriši pogovore in uporabnike na strani, nastavi spremenljivko currentRoom, nastavi spremenljivko nextMessageId na 0)
  // V razmislek: pomisli o morebitnih težavah!
  var changeRoom = function (event) {
    $("#messages").html("");
     $("#users").html("");
     currentRoom = event.currentTarget.getElementsByTagName("h5")[0].innerHTML;
     nextMessageId = 0;
  };
  // On Click handler za menjavo sobe
  // Namig: Seznam sob se lahko naloži kasneje, kot koda, ki se izvede tu.


});


